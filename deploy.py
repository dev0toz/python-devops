#!/usr/bin/python
# coding: utf-8

import os
import sys
import jenkins
import time


try:  
   os.environ["JENKINS_USER"]
except KeyError: 
   print("Please set the environment variable JENKINS_USER")
   sys.exit(1)

try:  
   os.environ["JENKINS_PWRD"]
except KeyError: 
   print("Please set the environment variable JENKINS_PWRD")
   sys.exit(1)

server = jenkins.Jenkins('https://jenkins.us.gitlabdemo.cloud', username=os.environ["JENKINS_USER"], password=os.environ["JENKINS_PWRD"])
job_name = 'fjdiaz/print-success'

server.build_job(job_name)
print("%s Job Started \n" % job_name)

# wait until last build is complete and then get the result
last_build_number = server.get_job_info(job_name)['lastCompletedBuild']['number']
next_build_number = server.get_job_info(job_name)['nextBuildNumber']
build_info = server.get_build_info(job_name, last_build_number)

# try max 2 mins before timing our
timeout = time.time() + 60 * 2
while last_build_number != next_build_number:
    if time.time() > timeout:
        sys.exit(1)
    last_build_number = server.get_job_info(job_name)['lastCompletedBuild']['number']
    build_info = server.get_build_info(job_name, last_build_number)

result = build_info["result"]
print("RESULT: %s" % result)

if result != "SUCCESS":
    print("Build Failed")
    sys.exit(1)

sys.exit(0)	
