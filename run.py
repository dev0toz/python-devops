import os
from notes import note


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    host = str(os.environ.get("HOST", 127.0.0.1))
    note.run(host=host, port=port)
